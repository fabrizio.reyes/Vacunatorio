<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Paciente;

/**
 * PacienteSearch represents the model behind the search form of `app\models\Paciente`.
 */
class PacienteSearch extends Paciente
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pac_codigo', 'pac_telefono', 'pac_del'], 'integer'],
            [['pac_nombre', 'pac_rut', 'pac_direccion', 'pac_correo', 'pac_fecha_nac', 'pac_nombre_resp'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Paciente::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pac_codigo' => $this->pac_codigo,
            'pac_telefono' => $this->pac_telefono,
            'pac_fecha_nac' => $this->pac_fecha_nac,
            'pac_del' => $this->pac_del,
        ]);

        $query->andFilterWhere(['like', 'pac_nombre', $this->pac_nombre])
            ->andFilterWhere(['like', 'pac_rut', $this->pac_rut])
            ->andFilterWhere(['like', 'pac_direccion', $this->pac_direccion])
            ->andFilterWhere(['like', 'pac_correo', $this->pac_correo])
            ->andFilterWhere(['like', 'pac_nombre_resp', $this->pac_nombre_resp]);

        return $dataProvider;
    }
}
