<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cita;

/**
 * CitaSearch represents the model behind the search form of `app\models\Cita`.
 */
class CitaSearch extends Cita
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cit_codigo', 'cit_del', 'pac_codigo', 'vac_codigo', 'id'], 'integer'],
            [['cit_fecha', 'cit_estado', 'cit_fecha_creac', 'cit_hora'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cita::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cit_codigo' => $this->cit_codigo,
            'cit_fecha' => $this->cit_fecha,
            'cit_fecha_creac' => $this->cit_fecha_creac,
            'cit_del' => $this->cit_del,
            'pac_codigo' => $this->pac_codigo,
            'vac_codigo' => $this->vac_codigo,
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'cit_estado', $this->cit_estado])
            ->andFilterWhere(['like', 'cit_hora', $this->cit_hora]);

        return $dataProvider;
    }
}
