<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "cita".
 *
 * @property int $cit_codigo
 * @property string $cit_fecha
 * @property string $cit_estado
 * @property string $cit_fecha_creac
 * @property int $cit_del
 * @property int $pac_codigo
 * @property int $vac_codigo
 * @property int $id
 *
 * @property Vacuna $vacCodigo
 * @property Paciente $pacCodigo
 * @property Usuario $id0
 */
class Cita extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cita';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cit_fecha', 'cit_estado', 'cit_fecha_creac','cit_hora', 'pac_codigo', 'vac_codigo', 'id'], 'required'],
               [['cit_fecha', 'cit_fecha_creac', 'cit_hora'], 'safe'],
            [['pac_codigo', 'vac_codigo', 'id'], 'integer'],
            [['cit_estado'], 'string', 'max' => 15],
            [['vac_codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Vacuna::className(), 'targetAttribute' => ['vac_codigo' => 'vac_codigo']],
            [['pac_codigo'], 'exist', 'skipOnError' => true, 'targetClass' => Paciente::className(), 'targetAttribute' => ['pac_codigo' => 'pac_codigo']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
           'cit_codigo' => 'Codigo',
            'cit_fecha' => 'Fecha de la cita',
            'cit_estado' => 'Estado',
            'cit_fecha_creac' => 'Fecha Creación',
            'cit_hora' => 'Hora',
            'cit_del' => 'Borrado',
            'pac_codigo' => 'Paciente',
            'vac_codigo' => 'Vacuna',
            'id' => 'id:',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
	
	
    public function getVacCodigo()
    {
        return $this->hasOne(Vacuna::className(), ['vac_codigo' => 'vac_codigo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPacCodigo()
    {
        return $this->hasOne(Paciente::className(), ['pac_codigo' => 'pac_codigo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'id']);
    }
}
