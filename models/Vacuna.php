<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vacuna".
 *
 * @property int $vac_codigo
 * @property string $vac_nombre
 * @property string $vac_tipo
 * @property string $vac_descripcion
 * @property int $vac_stock
 * @property string $vac_dosis
 * @property int $vac_del
 *
 * @property Cita[] $citas
 */
class Vacuna extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vacuna';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vac_nombre', 'vac_tipo', 'vac_descripcion', 'vac_stock', 'vac_dosis', 'vac_del'], 'required'],
            [['vac_stock', 'vac_del'], 'integer'],
            [['vac_nombre', 'vac_dosis'], 'string', 'max' => 200],
            [['vac_tipo'], 'string', 'max' => 100],
            [['vac_descripcion'], 'string', 'max' => 500],
			['vac_nombre', 'unique'],
			['vac_stock', 'compare', 'compareValue' => 0, 'operator' => '>','message'=>Yii::t('app','El stock no puede ser negativo')],
        ];
    }

    /**
     * {@inheritdoc}
     */
	
	public function getDel(){
		return $this->vac_del ? 1 : 0 ;
	}
	
    public function attributeLabels()
    {
        return [
           'vac_codigo' => 'Codigo:',
            'vac_nombre' => 'Nombre:',
            'vac_tipo' => 'Tipo:',
            'vac_descripcion' => 'Descripción:',
            'vac_stock' => 'Stock:',
            'vac_dosis' => 'Dosis:',
            'vac_del' => 'Borrado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCitas()
    {
        return $this->hasMany(Cita::className(), ['vac_codigo' => 'vac_codigo']);
    }
}
