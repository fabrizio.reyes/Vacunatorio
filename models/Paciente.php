<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paciente".
 *
 * @property int $pac_codigo
 * @property string $pac_nombre
 * @property string $pac_rut
 * @property int $pac_telefono
 * @property string $pac_direccion
 * @property string $pac_correo
 * @property string $pac_fecha_nac
 * @property string $pac_nombre_resp
 * @property int $pac_del
 *
 * @property Cita[] $citas
 */
class Paciente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paciente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pac_nombre', 'pac_rut', 'pac_telefono', 'pac_direccion', 'pac_correo', 'pac_fecha_nac', 'pac_del'], 'required'],
            [['pac_telefono', 'pac_del'], 'integer'],
            [['pac_fecha_nac'], 'safe'],
            [['pac_nombre', 'pac_nombre_resp'], 'string', 'max' => 50],
            [['pac_rut'], 'string', 'max' => 12],
            [['pac_direccion'], 'string', 'max' => 100],
            [['pac_correo'], 'string', 'max' => 255],
            ['pac_correo', 'email'],
			['pac_rut', 'unique'],
			['pac_rut','match', 'pattern' => '/^(\d{1}|\d{2})\.(\d{3}\.\d{3}-)([kK]{1}$|\d{1}$)/','message'=>Yii::t('app','RUT invalido, por favor ingrese el RUT en formato: 99.999.999-9')], 
				
			
			 /* ['pac_rut', 'validateRut'], */
			
        ];
    }

	protected function formatearRut($unformattedRut) {
            if (strpos($unformattedRut, '-') !== false ) {
                $splittedRut = explode('-', $unformattedRut);
                $number = number_format($splittedRut[0], 0, ',', '.');
                $verifier = strtoupper($splittedRut[1]);
                return $number . '-' . $verifier;
            }
          return number_format($unformattedRut, 0, ',', '.');
        }

/* public function validateRut($attribute, $params) {
        $data = explode('-', $this->rut);
        $evaluate = strrev($data[0]);
        $multiply = 2;
        $store = 0;
        for ($i = 0; $i < strlen($evaluate); $i++) {
            $store += $evaluate[$i] * $multiply;
            $multiply++;
            if ($multiply > 7)
                $multiply = 2;
        }
        isset($data[1]) ? $verifyCode = strtolower($data[1]) : $verifyCode = '';
        $result = 11 - ($store % 11);
        if ($result == 10)
            $result = 'k';
        if ($result == 11)
            $result = 0;
        if ($verifyCode != $result)
            $this->addError('rut', 'Rut inválido.');
    } */
	
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pac_codigo' => 'Codigo',
            'pac_nombre' => 'Nombre',
            'pac_rut' => 'Rut',
            'pac_telefono' => 'Telefono',
            'pac_direccion' => 'Direccion',
            'pac_correo' => 'Correo',
            'pac_fecha_nac' => 'Fecha de nacimiento',
            'pac_nombre_resp' => 'Nombre del responsable',
            'pac_del' => 'Responsable',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCitas()
    {
        return $this->hasMany(Cita::className(), ['pac_codigo' => 'pac_codigo']);
    }
}
