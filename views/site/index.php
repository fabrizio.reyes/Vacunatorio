<?php

use yii\helpers\Html;
use app\models\Usuario;



/* @var $this yii\web\View yyfvgbuhnjm*/


$this->title = 'Vacunatorio';
if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
	$session = Yii::$app->session;
	$session->set('pacient',0);
	$session->set('vacuna',0);
	$session->set('cita',0);
	$session->set('hay_cit',0);
	$session->set('user',0);
 
?>

<div class="site-index" >
</br>
</br>
</br>
<?php 




if(Yii::$app->user->identity->usu_tipo=='Administrador/a'){
	$u=true;
}else{
	$u=false;
}
?>
<div class="col-sm-3">
<img src="/Vacunatorio/web/images/vacuna.svg" style="padding-bottom: 15px">
<?= Html::a('Ingresar Vacuna', ['/vacuna/create'],['class'=>'btn btn-primary','style'=>'display: block']) ?>
<?= Html::a('Ver Vacunas', ['/vacuna/'],['class'=>'btn btn-primary','style'=>'display: block; ']) ?>
</div>
<div class="col-sm-3">
<img src="/Vacunatorio/web/images/paciente.svg" style="padding-bottom: 15px">
<?= Html::a('Ingresar Paciente', ['/paciente/create'],[''=>'','class'=>'btn btn-primary','style'=>'display: block']) ?>
<?= Html::a('Ver Pacientes', ['/paciente/'],['class'=>'btn btn-primary','style'=>'display: block']) ?>
</div>
<div class="col-sm-3">
<img src="/Vacunatorio/web/images/cita.svg" style="padding-bottom: 15px">
<?= Html::a('Ingresar Cita', ['/cita/create'],[''=>'','class'=>'btn btn-primary','style'=>'display: block']) ?>
<?= Html::a('Ver Citas', ['/cita/'],['class'=>'btn btn-primary','style'=>'display: block']) ?>
</div>
<div class="col-sm-3">
<img src="/Vacunatorio/web/images/usuario.svg" style="padding-bottom: 15px">
<?php if($u){ ?>
<?= Html::a('Crear Usuario', ['/usuario/create'],[''=>'','class'=>'btn btn-primary','style'=>'display: block']) ?>
<?= Html::a('Ver Usuarios', ['/usuario/'],['class'=>'btn btn-primary','style'=>'display: block']) ?>
<?php } ?>
<?= Html::a('Editar Mi Perfil', ['/usuario/modificate/'.Yii::$app->user->identity->id],['class'=>'btn btn-primary','style'=>'display: block']) ?>
</div>

