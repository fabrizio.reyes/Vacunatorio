<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Vacunatorio';

?>

	
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal'
    ]); ?>
	
	
	<html lang="en">
	<head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- CSS -->
		
        <link rel="stylesheet" href="/Vacunatorio/web/form-1/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/Vacunatorio/web/form-1/assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="/Vacunatorio/web/form-1/assets/css/form-elements.css">
        <link rel="stylesheet" href="/Vacunatorio/web/form-1/assets/css/style.css">
    </head>


	<body >

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                  <div class="container" >
                    <div class="row" >
                        <div style="color:black;" >
                            <h1><strong>Vacunatorio</strong></h1>
                            <div style="color:black;" >
                            	<h3>
	                            	Bienvenido al Sistema de Apoyo a Vacunatorio.
                            	</h3>
                            </div>
                        </div>
                    </div>
                      <div class="row" >
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3 align="center">Inicio de sesión</h3>
                            		<p>Ingrese su nombre de usuario y contraseña.</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-key"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="" method="post" class="login-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Username</label>
			                        	<?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Password</label>
			                        	<?= $form->field($model, 'password')->passwordInput() ?>
			                        </div>
			                        <?= Html::submitButton('Iniciar Sesión', ['class' => 'btn btn-primary col-lg-3', 'name' => 'login-button']) ?>
									 <?php ActiveForm::end(); ?>
			                    </form>
		                    </div>
                        </div>
                    </div>
					

                </div>
					
            </div>
            
        </div>

	
        <!-- Javascript -->
        <script src="/Vacunatorio/web/form-1/assets/js/jquery-1.11.1.min.js"></script>
        <script src="/Vacunatorio/web/form-1/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="/Vacunatorio/web/form-1/assets/js/jquery.backstretch.min.js"></script>
        <script src="/Vacunatorio/web/form-1/assets/js/scripts.js"></script>
        
    </body>
	
</html>			