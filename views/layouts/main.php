<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

if(Yii::$app->request->url!='/vacunatorio/web/index.php/site/login'){
		if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
}
	
AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>

<?php

	
$script = <<< JS
  $("#menu-close").click(function(e) {
    e.preventDefault();
    $("#sidebar-wrapper").toggleClass("active");
  });
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#sidebar-wrapper").toggleClass("active");
  });
JS;
$this->registerJs($script);
?> 
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body background="/Vacunatorio/web/images/VACUNATORIO.png">
<?php $this->beginBody() ?>

<div class="wrap" >
    <?php
	if(Yii::$app->request->url!='/vacunatorio/web/index.php/site/login'){
		
    NavBar::begin([
        'brandLabel' =>'Vacunatorio',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
	if(Yii::$app->user->isGuest){
		echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            //['label' => 'Home', 'url' => ['/site/index']],
            //['label' => 'About', 'url' => ['/site/about']],
            //['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Iniciar Sesión', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Cerrar Sesión (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
		
		
	}else{
		
	echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => [
			Yii::$app->user->identity->usu_tipo=='Administrador/a' ? (
				'<p class="navbar-text" style="margin-top: 3px;margin-bottom: 0px;">Administrador/a </p>'
			) : (
				'<p class="navbar-text" style="margin-top: 3px;margin-bottom: 0px;">Secetario/a </p>'
			),
			'<div class="flex-column">
				<div>
					<h4 class="navbar-text" style="margin-top: 0px;margin-bottom: 0px;">'. Yii::$app->user->identity->usu_nombre.'</h4>
				</div>
				
			</div>',
			
        ],
    ]);
	
		echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
			
			
			Yii::$app->user->isGuest ? (
                ['label' => 'Iniciar Sesión', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Cerrar Sesión (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout','style' => 'padding-top: 15px']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
	}
	
    NavBar::end();
	}
    ?>
	

    <div class="container" style="margin-top: 50px;padding-top: 0px;">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer"  style="background: #8787cc;">
    <div class="container">
        

       
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
