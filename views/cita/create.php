<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Cita */
echo Html::a('Ingresar Paciente', ['/paciente/create'],[''=>'','class'=>'btn btn-primary','style'=>'display: block']);
$this->title = 'Ingresar Cita';
if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
	$session = Yii::$app->session;
	if($session->get('pacient')){ ?>
		<script>alert("Paciente creado correctamente"); </script>
		<?php 
			$session->set('pacient',0);
		} 
		$session->set('cita',1);
		
		if($session->get('hay_cit')){ ?>
		<script>alert("Ya existe una cita agendada a esta hora el mismo día"); </script>
		<?php 
			$session->set('hay_cit',0);
		} 
		?>
		

<div class="cita-create">

    <h1><?= Html::encode($this->title) ?></h1>
	
    <?= $this->render('_form_create', [
        'model' => $model,
    ]) ?>

</div>
