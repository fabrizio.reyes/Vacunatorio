<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CitaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
if(Yii::$app->user->identity->usu_tipo=='Administrador/a'){
?>
<?= Html::a("Generar Informe de Citas por Mes",['/cita/indexpdf'],['class'=>'btn btn-primary col-xs-4', 'style' => 'margin-bottom: 10px; background-color:orange;color:black;']) ?>

<?= Html::a("Generar Informe de Citas por Pacientes",['/cita/indexpdf2'],['class'=>'btn btn-primary col-xs-4', 'style' => 'margin-bottom: 10px; background-color:orange;color:black;']) ?>

<?= Html::a("Generar Informe de Citas por Vacunas",['/cita/indexpdf3'],['class'=>'btn btn-primary col-xs-4', 'style' => 'margin-bottom: 10px; background-color:orange;color:black;']) ?>
				<?php
}
$this->title = 'Citas';
if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
	
	$session = Yii::$app->session;
	if($session->get('cita')){ ?>
		<script>alert("Cita creada correctamente"); </script>
		<?php 
			$session->set('cita',0);
		} ?>

</br>
<div class="cita-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	
	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'cit_codigo',
            'cit_fecha',
            'cit_estado',
            //'cit_fecha_creac',
            'cit_hora',
            //'cit_del',
            //'pac_codigo',
            //'vac_codigo',
            //'id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<?= Html::a("<i class='glyphicon glyphicon-chevron-left'></i>",
				Yii::$app->homeUrl,['class'=>'btn btn-primary col-xs-12', 'style' => 'margin-bottom: 10px']) ?>