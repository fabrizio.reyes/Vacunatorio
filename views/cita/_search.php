<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CitaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cita-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cit_codigo') ?>

    <?= $form->field($model, 'cit_fecha') ?>

    <?= $form->field($model, 'cit_estado') ?>

    <?= $form->field($model, 'cit_fecha_creac') ?>

    <?= $form->field($model, 'cit_hora') ?>

    <?php // echo $form->field($model, 'cit_del') ?>

    <?php // echo $form->field($model, 'pac_codigo') ?>

    <?php // echo $form->field($model, 'vac_codigo') ?>

    <?php // echo $form->field($model, 'id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
