<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Mpdf\Mpdf;
use app\models\Paciente;
use app\models\Vacuna;
use app\models\Cita;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CitaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$citas=Cita::find()->orderby("pac_codigo")->all();


$this->title = 'Citas';
if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
	
	$session = Yii::$app->session;
	if($session->get('cita')){ ?>
		<script>alert("Cita creada correctamente"); </script>
		<?php 
			$session->set('cita',0);
		} 
		$mpdf = new \Mpdf\Mpdf();
		ob_start();
		?>

</br>
<div class="cita-index">

    <h1><?= Html::encode($this->title) ?></h1>
 
	
	
   	<table class="table">
    <thead>
      <tr>
	  <th>Nombre del Paciente</th>
		<th>Rut del Paciente</th>
        <th>Celular del Paciente</th>
        <th>Fecha de la Cita </th>
        <th>Hora de la Cita </th>
        <th>Vacuna</th>		
      </tr>
    </thead>
    <tbody>
	<?php for($i=0;$i<sizeof($citas);$i++){
		
		$paciente=Paciente::find()->where('pac_codigo='.$citas[$i]->pac_codigo)->one();
		$vacuna=Vacuna::find()->where('vac_codigo='.$citas[$i]->vac_codigo)->one();
		
		$m1=0;$m2=0;
		
	$m1=$citas[$i]->pac_codigo;
	if($i>0){
	$m2=$citas[$i-1]->pac_codigo;
	}
	if($m1!=$m2){
		?>
		<tr>
		<td>
		-
		</td>
		</tr>
		<tr>
		<td>
		<?= '<b>'.$paciente->pac_nombre.'</b>' ?>
		</td>
		</tr>
		<?php } ?>
      <tr>
		<td><?= $paciente->pac_nombre ?></td>
        <td><?= $paciente->pac_rut ?></td>
        <td><?= $paciente->pac_telefono ?></td>
        <td><?= $citas[$i]->cit_fecha ?></td>
        <td><?= $citas[$i]->cit_hora ?></td>
        <td><?= $vacuna->vac_nombre ?></td>
      </tr>
	<?php } ?>
    </tbody>
  </table>
	
	
	<?php
$html = ob_get_contents();
ob_end_clean();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
?>
</div>
<?= Html::a("<i class='glyphicon glyphicon-chevron-left'></i>",
				Yii::$app->homeUrl,['class'=>'btn btn-primary col-xs-12', 'style' => 'margin-bottom: 10px']) ?>