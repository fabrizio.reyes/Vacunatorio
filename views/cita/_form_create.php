<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\TimePicker;
use app\models\Paciente;
use app\models\Vacuna;


$pacientes=ArrayHelper::map(Paciente::find()->All(), 'pac_codigo', 'pac_nombre');
$vacunas=ArrayHelper::map(Vacuna::find()->All(), 'vac_codigo', 'vac_nombre');





/* @var $this yii\web\View */
/* @var $model app\models\Cita */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="cita-form">

    <?php $form = ActiveForm::begin(["action"=>$_SERVER['SCRIPT_NAME'].'/cita/create']); ?>

    <div class="panel panel-primary">
		<div class="panel-body" >
			<div class="col-xs-9 form-horizontal" style="width: 40%" >
				<?= $form->field($model, 'pac_codigo')->dropDownList($pacientes, 
						 ['prompt'=>'-Seleccione un Paciente-',
						  'onchange'=>'
							$.post( "'.Yii::$app->urlManager->createUrl('post/lists?id=').'"+$(this).val(), function( data ) {
							  $( "select#title" ).html( data );
							});
						'])
				?>
				
				<?= $form->field($model, 'cit_fecha')->widget(DatePicker::classname(), [
					'options' => ['placeholder' => 'Elija la fecha de la cita','value' =>date('Y-m-d') ],
					'readonly' => true,
					'pluginOptions' => [
						'autoclose'=>true,
						'format' => 'yyyy-mm-dd',
						'startDate' => date('Y-m-d')
					 ]]) ?>
				
			</div>
			<div class="col-xs-9 form-horizontal" style="width: 10%" >
			</div>
			<div class="col-xs-9 form-horizontal"style="width: 40%" >
				<?= $form->field($model, 'vac_codigo')->dropDownList($vacunas, 
						 ['prompt'=>'-Seleccione una Vacuna-',
						  'onchange'=>'
							$.post( "'.Yii::$app->urlManager->createUrl('post/lists?id=').'"+$(this).val(), function( data ) {
							  $( "select#title" ).html( data );
							});
						'])
				?>
				<?= $form->field($model, 'cit_hora')->widget(TimePicker::classname(),[
				'name' => 't1',
				'readonly' => true,
				'pluginOptions' => [
					'showMeridian' => false,
					
				]]) ?>	
			</div>		
		</div>		
	</div>
	<div class="col-lg-12">
		<div class="row">
			<?= Html::a("<i class='glyphicon glyphicon-chevron-left'></i>",
				Yii::$app->homeUrl,['class'=>'btn btn-primary col-xs-5', 'style' => 'margin-bottom: 10px']) ?>
			<div class="spacer col-xs-2"></div>		
			
			<?= Html::submitButton('Ingresar', ['class' => 'btn btn-success col-xs-5', 'style' => 'margin-bottom: 10px']) ?>	
			
		</div>
	</div>
    <?php ActiveForm::end(); ?>

</div>
