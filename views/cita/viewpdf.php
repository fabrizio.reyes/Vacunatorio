<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Paciente;
use app\models\Vacuna;
use app\models\Usuario;
use Mpdf\Mpdf;

if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}

$paciente=Paciente::find()->where('pac_codigo='.$model->pac_codigo)->one();
$vacuna=Vacuna::find()->where('vac_codigo='.$model->vac_codigo)->one();
$usuario=Usuario::find()->where('id='.$model->id)->one();

/* @var $this yii\web\View */
/* @var $model app\models\Cita */
$model2=[
			'Fecha de Cita' => $model->cit_fecha,
            'Estado' => $model->cit_estado,
            'Fecha de creacion' => $model->cit_fecha_creac,
			'Hora' => $model->cit_hora,
            'Nombre Paciente' => $paciente->pac_nombre,
            'Nombre Vacuna' => $vacuna->vac_nombre,
			'Ingresada por' => $usuario->usu_nombre,
];
$mpdf = new \Mpdf\Mpdf();
		ob_start();
$this->title = 'Datos de la Cita';
	
?>
</br>
<div class="cita-view">

    <h1><?= Html::encode($this->title) ?></h1>

  

    <?= DetailView::widget([
        'model' => $model2,
        'attributes' => [
            'Fecha de Cita',
            'Estado',
            'Fecha de creacion',
			'Hora',
            'Nombre Paciente',
            'Nombre Vacuna',
			'Ingresada por',
        ],
		/* [                      // the owner name of the model
            'label' => 'Owner',
            'value' => $paciente->pac_codigo,
        ], */
    ]) 
	
	?>
	
	<?php
$html = ob_get_contents();
ob_end_clean();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
?>
 
		
</div>
<?= Html::a("<i class='glyphicon glyphicon-chevron-left'></i>", ['/cita/'],['class'=>'btn btn-primary	col-xs-12']) ?>
