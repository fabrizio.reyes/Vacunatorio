<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\TimePicker;
use app\models\Paciente;
use app\models\Vacuna;

/* @var $this yii\web\View */
/* @var $model app\models\Cita */
/* @var $form yii\widgets\ActiveForm */

$estados=['Pendiente' =>'Pendiente','Realizada' =>'Realizada','No realizada' =>'No realizada'];

$pacientes=ArrayHelper::map(Paciente::find()->All(), 'pac_codigo', 'pac_nombre');
$vacunas=ArrayHelper::map(Vacuna::find()->All(), 'vac_codigo', 'vac_nombre');

?>

<div class="cita-form">

    <?php $form = ActiveForm::begin(); ?>

     <?= $form->field($model, 'cit_fecha')->widget(DatePicker::classname(), [
					'options' => ['placeholder' => $model->cit_fecha],
					'readonly' => true,
					'pluginOptions' => [
						'autoclose'=>true,
						'format' => 'yyyy-mm-dd',
						'startDate' => date('Y-m-d')
					 ]]) ?>

    <?= $form->field($model, 'cit_estado')->dropDownList($estados, 
             ['placeholder' => $model->cit_estado,
              'onchange'=>'
                $.post( "'.Yii::$app->urlManager->createUrl('post/lists?id=').'"+$(this).val(), function( data ) {
                  $( "select#title" ).html( data );
                });
            '])
	?>

    
	
	 <?= $form->field($model, 'cit_hora')->widget(TimePicker::classname(),[
				'name' => 't1',
				'readonly' => true,
				'pluginOptions' => [
					'showMeridian' => false,
					
				]]) ?>		

    

    <?= $form->field($model, 'pac_codigo')->dropDownList($pacientes, 
						 ['prompt'=>'-Seleccione un Paciente-',
						  'onchange'=>'
							$.post( "'.Yii::$app->urlManager->createUrl('post/lists?id=').'"+$(this).val(), function( data ) {
							  $( "select#title" ).html( data );
							});
						'])
				?>

    <?= $form->field($model, 'vac_codigo')->dropDownList($vacunas, 
						 ['prompt'=>'-Seleccione una Vacuna-',
						  'onchange'=>'
							$.post( "'.Yii::$app->urlManager->createUrl('post/lists?id=').'"+$(this).val(), function( data ) {
							  $( "select#title" ).html( data );
							});
						'])
				?>

    
<?= $form->field($model, 'cit_del')->hiddenInput()->label(false) ?>
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success col-xs-12']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>