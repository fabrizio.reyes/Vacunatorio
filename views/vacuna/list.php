<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Vacuna;

$vacuna=Vacuna::find()->all();

/* @var $this yii\web\View */
/* @var $model app\models\Vacuna */
/* @var $form yii\widgets\ActiveForm */
if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
?>

<div class="vacuna-form">

 <?php
 for($i=0;$i<sizeof($vacuna);$i++){ 
	 ?>
	 <?= Html::a($vacuna[$i]->vac_nombre, ['/vacuna/update/'.$vacuna[$i]->vac_codigo],[''=>'','class'=>'btn btn-lg btn-success']) ?>
	 </br>
 <?php
	 }
 ?>

</div>
