<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use Mpdf\Mpdf;
/* @var $this yii\web\View */
/* @var $model app\models\Vacuna */



$this->title = 'Datos de la Vacuna';

//$model->vac_nombre='prueba';
//$model->save();

if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
	
	

?>

<div class="vacuna-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'vac_codigo',
            'vac_nombre',
            'vac_tipo',
            'vac_descripcion',
            'vac_stock',
            'vac_dosis',
            //'vac_del',
        ],
    ]) ?>
</div>

<?= Html::a("<i class='glyphicon glyphicon-chevron-left'></i>", ['/vacuna/'],['class'=>'btn btn-primary	col-xs-12']) ?>