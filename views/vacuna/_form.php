<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Vacuna */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vacuna-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vac_nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vac_tipo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vac_descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vac_stock')->textInput() ?>

    <?= $form->field($model, 'vac_dosis')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vac_del')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success col-xs-12']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
