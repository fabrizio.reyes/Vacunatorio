<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Vacuna */

$this->title = 'Ingresar Vacuna';
if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
	$session = Yii::$app->session;
	$session->set('vacuna',1);
?>
<div class="vacuna-create">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form_create', [
        'model' => $model,
    ]) ?>

</div>
