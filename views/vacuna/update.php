<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Vacuna */

$this->title = 'Editar Vacuna: '.$model->vac_nombre;
if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
?>
</br>
<div class="vacuna-update">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="panel panel-primary">
		<div class="panel-body" >
			 <?= $this->render('_form', [
				'model' => $model,
			]) ?>	
		</div>		
	</div>
   

</div>
<?= Html::a("<i class='glyphicon glyphicon-chevron-left'></i>", ['/vacuna/'],
['class'=>'btn btn-primary col-xs-12', 'style' => 'margin-bottom: 10px']) ?>