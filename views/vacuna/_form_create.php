<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\vacuna */
/* @var $form yii\widgets\ActiveForm */

?>
<div class="paciente-form">
    <?php $form = ActiveForm::begin(["action"=>$_SERVER['SCRIPT_NAME'].'/vacuna/create']); ?>
	<div class="panel panel-primary">
		<div class="panel-body" >
			<div class="col-xs-9 form-horizontal" style="width: 40%" >
				<?= $form->field($model, 'vac_nombre')->textInput(['maxlength' => true]) ?>
				<?= $form->field($model, 'vac_tipo')->textInput(['maxlength' => true]) ?>
				<?= $form->field($model, 'vac_dosis')->textInput(['maxlength' => true]) ?>
				
			</div>
			<div class="col-xs-9 form-horizontal" style="width: 10%" >
			</div>
			<div class="col-xs-9 form-horizontal"style="width: 40%" >
				<?= $form->field($model, 'vac_descripcion')->textInput(['maxlength' => true]) ?>
				<?= $form->field($model, 'vac_stock')->textInput() ?>
			</div>		
		</div>		
	</div>
  
	<div class="col-lg-12">
		<div class="row">
			<?= Html::a("<i class='glyphicon glyphicon-chevron-left'></i>",
				Yii::$app->homeUrl,['class'=>'btn btn-primary col-xs-5', 'style' => 'margin-bottom: 10px']) ?>
			<div class="spacer col-xs-2"></div>		
			<?= Html::submitButton('Ingresar', ['class' => 'btn btn-success col-xs-5', 'style' => 'margin-bottom: 10px']) ?>			
		</div>
	</div>
	<?php ActiveForm::end(); ?>

</div>
