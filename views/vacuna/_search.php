<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VacunaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vacuna-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'vac_codigo') ?>

    <?= $form->field($model, 'vac_nombre') ?>

    <?= $form->field($model, 'vac_tipo') ?>

    <?= $form->field($model, 'vac_descripcion') ?>

    <?= $form->field($model, 'vac_stock') ?>

    <?php // echo $form->field($model, 'vac_dosis') ?>

    <?php // echo $form->field($model, 'vac_del') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
