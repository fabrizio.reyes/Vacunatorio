<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Vacuna;
/* @var $this yii\web\View */
/* @var $searchModel app\models\VacunaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vacunas';
if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
	$session = Yii::$app->session;
	if($session->get('vacuna')){ ?>
		<script>alert("Vacuna creada correctamente"); </script>
		<?php 
			$session->set('vacuna',0);
		} 
		$mpdf = new \Mpdf\Mpdf();
	ob_start();
?>
</br>

<div class="vacuna-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'vac_codigo',
            'vac_nombre',
            'vac_tipo',
            'vac_descripcion',
            'vac_stock',
            'vac_dosis',
            //'vac_del',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	<?php
$html = ob_get_contents();
ob_end_clean();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
?>
</div>
<?= Html::a("<i class='glyphicon glyphicon-chevron-left'></i>",
				Yii::$app->homeUrl,['class'=>'btn btn-primary col-xs-12', 'style' => 'margin-bottom: 10px']) ?>
