<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Usuario;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios';
if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
	
	$session = Yii::$app->session;
	if($session->get('user')){ ?>
		<script>alert("Usuario creado correctamente"); </script>
		<?php 
			$session->set('user',0);
		} 
$u=Yii::$app->user->identity->username;

/* print_r($sadmin);
echo '</br></br>';
print_r($admin);
echo '</br></br>';
print_r($sec);
echo '</br></br>';
print_r($dataProvider); */
//$sec1=Usuario::find()->where('username='.$u)->one();
//print_r($sec1);
/* if(Yii::$app->user->identity->usu_tipo==2){
	$dataProvider = array_merge($sadmin, $admin, $sec);
}
if(Yii::$app->user->identity->usu_tipo==1){
	$dataProvider = array_merge($admin, $sec);
}
if(Yii::$app->user->identity->usu_tipo==0){
	$dataProvider = $sec1;
}	 */
?>
</br>

<div class="usuario-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            'username',
            //'password',
            //'usu_telefono',
            'usu_nombre',
            'usu_tipo',
            //'usu_del',
            //'authKey',
            //'accessToken',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<?= Html::a("<i class='glyphicon glyphicon-chevron-left'></i>",Yii::$app->homeUrl,['class'=>'btn btn-primary col-xs-12', 'style' => 'margin-bottom: 10px']) ?>