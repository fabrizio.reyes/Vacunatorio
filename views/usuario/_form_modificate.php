<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="paciente-form">
    <?php $form = ActiveForm::begin(["action"=>$_SERVER['SCRIPT_NAME'].'/usuario/modificate/'.$model->id]); ?>	
	<div class="panel panel-primary">
		<div class="panel-body" >
			<div class="col-xs-9 form-horizontal" style="width: 40%" >
			
				<?= $form->field($model, 'usu_nombre')->textInput(['maxlength' => true]) ?>
				<?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
				<?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
				<?= $form->field($model, 'usu_telefono')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-xs-9 form-horizontal" style="width: 10%" >
			</div>
			<div class="col-xs-9 form-horizontal"style="width: 40%" >
				
				
			</div>		
		</div>		
	</div>
  
	<div class="col-lg-12">
		<div class="row">
			<?= Html::a("<i class='glyphicon glyphicon-chevron-left'></i>",
				Yii::$app->homeUrl,['class'=>'btn btn-primary col-xs-5', 'style' => 'margin-bottom: 10px']) ?>
			<div class="spacer col-xs-2"></div>		
			<?= Html::submitButton('Guardar', ['class' => 'btn btn-success col-xs-5', 'style' => 'margin-bottom: 10px']) ?>	
		</div>
	</div>

    <?php ActiveForm::end(); ?>

</div>
