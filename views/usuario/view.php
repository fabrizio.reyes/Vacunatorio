<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\models\Usuario */

$this->title = 'Datos del Usuario';

if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
?>
</br>
<div class="usuario-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'username',
            //'password',
            'usu_telefono',
            'usu_nombre',
            'usu_tipo',
            //'usu_del',
            //'authKey',
            //'accessToken',
        ],
    ]) ?>

</div>

<?= Html::a("<i class='glyphicon glyphicon-chevron-left'></i>", ['/usuario/'],['class'=>'btn btn-primary col-xs-12']) ?>