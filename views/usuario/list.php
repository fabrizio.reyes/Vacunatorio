<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Usuario;

$usuarios=Usuario::find()->all();

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
?>

<div class="cita-form">

 <?php
 for($i=0;$i<sizeof($usuarios);$i++){
	 
	 ?>
	 <?= Html::a($usuarios[$i]->username, ['/usuario/update/'.$usuarios[$i]->id],[''=>'','class'=>'btn btn-lg btn-success'])?>
	 </br>
 <?php
	 }
 ?>

</div>
