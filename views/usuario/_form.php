<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
$tipos=[
	'Secretario/a' => 'Secretario/a',
    'Administrador/a' => 'Administrador/a',
];
?>

<div class="usuario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'usu_telefono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'usu_nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'usu_tipo')->dropDownList($tipos, 
						 [
						  'onchange'=>'
							$.post( "'.Yii::$app->urlManager->createUrl('post/lists?id=').'"+$(this).val(), function( data ) {
							  $( "select#title" ).html( data );
							});
						'])
				?>
	
	<?= $form->field($model, 'usu_del')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'authKey')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'accessToken')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success col-xs-12']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
