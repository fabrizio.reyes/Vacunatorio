<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Paciente */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="paciente-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pac_nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pac_rut')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pac_telefono')->textInput() ?>

    <?= $form->field($model, 'pac_direccion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pac_correo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pac_fecha_nac')->widget(DatePicker::classname(), [
					'options' => ['placeholder' => $model->pac_fecha_nac],
					'readonly' => true,
					'pluginOptions' => [
						'autoclose'=>true,
						'format' => 'yyyy-mm-dd',
						'startDate' => date('Y-m-d')
					 ]]) ?>

    <?= $form->field($model, 'pac_nombre_resp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pac_del')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success col-xs-12']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
