<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PacienteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="paciente-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'pac_codigo') ?>

    <?= $form->field($model, 'pac_nombre') ?>

    <?= $form->field($model, 'pac_rut') ?>

    <?= $form->field($model, 'pac_telefono') ?>

    <?= $form->field($model, 'pac_direccion') ?>

    <?php // echo $form->field($model, 'pac_correo') ?>

    <?php // echo $form->field($model, 'pac_fecha_nac') ?>

    <?php // echo $form->field($model, 'pac_nombre_resp') ?>

    <?php // echo $form->field($model, 'pac_del') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
