<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\mpdf\Pdf;
use app\models\Paciente;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use Mpdf\Mpdf;


/* @var $this yii\web\View */
/* @var $searchModel app\models\PacienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pacientes';
if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
	
	$mpdf = new \Mpdf\Mpdf();
	ob_start();
?>
 


</br>

<div class="paciente-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'pac_codigo',
            'pac_nombre',
            'pac_rut',
            //'pac_telefono',
            //'pac_direccion',
            'pac_correo',
            'pac_fecha_nac',
            'pac_nombre_resp',
            //'pac_del',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php
$html = ob_get_contents();
ob_end_clean();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
?>
</div>
<?= Html::a("<i class='glyphicon glyphicon-chevron-left'></i>",
				Yii::$app->homeUrl,['class'=>'btn btn-primary col-xs-12', 'style' => 'margin-bottom: 10px']) ?>
