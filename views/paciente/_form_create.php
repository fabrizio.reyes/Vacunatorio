<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Cita;
use kartik\widgets\DatePicker;
use yii\widgets\Bootstrap;

/* @var $this yii\web\View */
/* @var $model app\models\Paciente */
/* @var $form yii\widgets\ActiveForm */
$resp=[
	0 => 'Mismo Paciente',
    1 => 'Otro',
];
?>
<div class="paciente-form">
	<?php $form = ActiveForm::begin(["action"=>$_SERVER['SCRIPT_NAME'].'/paciente/create']); ?>
	<div class="panel panel-primary">
		<div class="panel-heading">Datos del Paciente</div>
		<div class="panel-body" >
			<div class="col-xs-9 form-horizontal" style="width: 40%" >
				<?= $form->field($model, 'pac_nombre')->textInput(['maxlength' => true, 'placeholder' =>'Ingrese el nombre del Paciente']) ?>
				<?= $form->field($model, 'pac_rut')->textInput(['maxlength' => true,'placeholder' =>'Ej: 99.999.999-9']) ?>
			</div>
			<div class="col-xs-9 form-horizontal" style="width: 10%" >
			</div>
			<div class="col-xs-9 form-horizontal"style="width: 40%" >
				<?= $form->field($model, 'pac_fecha_nac')->widget(DatePicker::classname(), [
					'options' => ['placeholder' => 'Elija la fecha de nacimiento' ],
					'readonly' => true,
					'pluginOptions' => [
						'autoclose'=>true,
						'format' => 'yyyy-mm-dd'
					 ]]) ?>
			</div>		
		</div>		
	</div>
    <div class="panel panel-primary">
		<div class="panel-heading">Datos de Contacto</div>
		<div class="panel-body" >
			<div class="col-xs-9 form-horizontal" style="width: 40%" >
				<?= $form->field($model, 'pac_telefono')->textInput() ?>
			<?= $form->field($model, 'pac_direccion')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-xs-9 form-horizontal" style="width: 10%" >
			</div>
			<div class="col-xs-9 form-horizontal"style="width: 40%" >
				<?= $form->field($model, 'pac_correo')->textInput(['maxlength' => true,'placeholder' =>'ejemplo@correo.cl' ]) ?>
			<?= $form->field($model, 'pac_del')->dropDownList($resp, 
						 [
						  'onchange'=>'
							$.post( "'.Yii::$app->urlManager->createUrl('post/lists?id=').'"+$(this).val(), function( data ) {
							  $( "select#title" ).html( data );
							});
						'])
				?>
			<?= $form->field($model, 'pac_nombre_resp')->textInput(['maxlength' => true,'placeholder' =>'No es necesario si marcó Mismo Paciente']) ?>
			</div>		
		</div>		
	</div>
  
	<div class="col-lg-12">
		<div class="row">
			<?= Html::a("<i class='glyphicon glyphicon-chevron-left'></i>",
				Yii::$app->homeUrl,['class'=>'btn btn-primary col-xs-5', 'style' => 'margin-bottom: 10px']) ?>
			<div class="spacer col-xs-2"></div>		
			
			<?= Html::submitButton('Ingresar', ['class' => 'btn btn-success col-xs-5', 'style' => 'margin-bottom: 10px']) ?>	
			
		</div>
	</div>
    <?php ActiveForm::end(); ?>
</div>
