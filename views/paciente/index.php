<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\mpdf\Pdf;
use app\models\Paciente;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$pacientes=ArrayHelper::map(Paciente::find()->All(), 'pac_codigo', 'pac_nombre');
/* @var $this yii\web\View */
/* @var $searchModel app\models\PacienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
if(Yii::$app->user->identity->usu_tipo=='Administrador/a'){
?>
<?= Html::a("Generar Informe de Pacientes",['/paciente/indexpdf'],['class'=>'btn btn-primary col-xs-12', 'style' => 'margin-bottom: 10px; background-color:orange;color:black;']) ?>

				<?php
}
				$this->title = 'Pacientes';
if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
	
	
?>
 


</br>

<div class="paciente-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'pac_codigo',
            'pac_nombre',
            'pac_rut',
            //'pac_telefono',
            //'pac_direccion',
            'pac_correo',
            'pac_fecha_nac',
            'pac_nombre_resp',
            //'pac_del',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
<?= Html::a("<i class='glyphicon glyphicon-chevron-left'></i>",
				Yii::$app->homeUrl,['class'=>'btn btn-primary col-xs-12', 'style' => 'margin-bottom: 10px']) ?>
