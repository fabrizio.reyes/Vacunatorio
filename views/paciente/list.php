<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Paciente;

$pacientes=Paciente::find()->all();

/* @var $this yii\web\View */
/* @var $model app\models\Paciente */
/* @var $form yii\widgets\ActiveForm */
if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
?>

<div class="paciente-form">

 <?php
 for($i=0;$i<sizeof($pacientes);$i++){
	 
	 ?>
	 <?= Html::a($pacientes[$i]->pac_nombre, ['/paciente/update/'.$pacientes[$i]->pac_codigo],[''=>'','class'=>'btn btn-lg btn-success']) ?>
	 </br>
 <?php
	 }
 ?>

</div>
