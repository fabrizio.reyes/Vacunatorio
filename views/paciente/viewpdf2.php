<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use Mpdf\Mpdf;
use app\models\Cita;
use app\models\Vacuna;

$citas=cita::find()->where('pac_codigo='.$model->pac_codigo)->all();
/* @var $this yii\web\View */
/* @var $model app\models\Paciente */

$this->title = 'Citas del Paciente: '.$model->pac_nombre;
//$this->title = $model -> pac_nombre;
if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
	$mpdf = new \Mpdf\Mpdf();
	ob_start();
?>
</br>

<div class="paciente-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <table class="table">
    <thead>
      <tr>
		<th>Fecha de la cita</th>
		<th>Hora</th>
        <th>Vacuna</th>
      </tr>
    </thead>
    <tbody>
	<?php for($i=0;$i<sizeof($citas);$i++){
		$vacuna=Vacuna::find()->where('vac_codigo='.$citas[$i]->vac_codigo)->one();
?>
      <tr>
        <td><?= $citas[$i]->cit_fecha ?></td>
        <td><?= $citas[$i]->cit_hora ?></td>
        <td><?= $vacuna->vac_nombre ?></td>
      </tr>
	<?php } ?>
    </tbody>
  </table>
  
<?php
$html = ob_get_contents();
ob_end_clean();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
?>
</div>
<?= Html::a("<i class='glyphicon glyphicon-chevron-left'></i>", ['/paciente/'],['class'=>'btn btn-primary col-xs-12']) ?>
