<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use Mpdf\Mpdf;

/* @var $this yii\web\View */
/* @var $model app\models\Paciente */

$this->title = 'Datos del Paciente';
//$this->title = $model -> pac_nombre;
if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
	$mpdf = new \Mpdf\Mpdf();
	ob_start();
?>
</br>

<div class="paciente-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'pac_codigo',
			'pac_nombre',
            'pac_rut',
            'pac_telefono',
            'pac_direccion',
            'pac_correo',
            'pac_fecha_nac',
            'pac_nombre_resp',
            //'pac_del',
        ],
    ]) ?>
<?php
$html = ob_get_contents();
ob_end_clean();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
?>
</div>
<?= Html::a("<i class='glyphicon glyphicon-chevron-left'></i>", ['/paciente/'],['class'=>'btn btn-primary col-xs-12']) ?>
