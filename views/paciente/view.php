<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use Mpdf\Mpdf;
/* @var $this yii\web\View */
/* @var $model app\models\Paciente */
if(Yii::$app->user->identity->usu_tipo=='Administrador/a'){
echo Html::a('Generar PDF',['/paciente/viewpdf/'.$model->pac_codigo], ['class' => 'btn btn-primary col-xs-6','style'=>'background-color:orange;color:black;']);

echo Html::a('Generar Informe de las citas',['/paciente/viewpdf2/'.$model->pac_codigo], ['class' => 'btn btn-primary col-xs-6','style'=>'background-color:orange;color:black;']);
$this->title = 'Datos del Paciente';
}
if(Yii::$app->user->isGuest){
  if (!empty($_SERVER['HTTPS']) && ('on' == $_SERVER['HTTPS'])) {
		$uri = 'https://';
	} else {
		$uri = 'http://';
	}
	$uri .= $_SERVER['HTTP_HOST'];
	header('Location: '.$uri.'/vacunatorio/web/index.php/site/login');
	exit; 
	}
	
?>
</br>

<div class="paciente-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'pac_codigo',
            'pac_nombre',
            'pac_rut',
            'pac_telefono',
            'pac_direccion',
            'pac_correo',
            'pac_fecha_nac',
            'pac_nombre_resp',
            //'pac_del',
        ],
    ]) ?>
</div>
<?= Html::a("<i class='glyphicon glyphicon-chevron-left'></i>", ['/paciente/'],['class'=>'btn btn-primary col-xs-12']) ?>
</br>
</br>
</br>
